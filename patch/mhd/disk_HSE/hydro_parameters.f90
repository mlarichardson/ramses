module hydro_parameters
  use amr_parameters

  ! Number of independant variables
#ifndef NENER
  integer,parameter::nener=0
#else
  integer,parameter::nener=NENER
#endif
#ifndef NVAR
  integer,parameter::nvar=8+nener
#else
  integer,parameter::nvar=NVAR
#endif

  ! Size of hydro kernel
  integer,parameter::iu1=-1
  integer,parameter::iu2=+4
  integer,parameter::ju1=(1-ndim/2)-1*(ndim/2)
  integer,parameter::ju2=(1-ndim/2)+4*(ndim/2)
  integer,parameter::ku1=(1-ndim/3)-1*(ndim/3)
  integer,parameter::ku2=(1-ndim/3)+4*(ndim/3)
  integer,parameter::if1=1
  integer,parameter::if2=3
  integer,parameter::jf1=1
  integer,parameter::jf2=(1-ndim/2)+3*(ndim/2)
  integer,parameter::kf1=1
  integer,parameter::kf2=(1-ndim/3)+3*(ndim/3)

  ! Imposed boundary condition variables
  real(dp),dimension(1:MAXBOUND,1:nvar+3)::boundary_var
  real(dp),dimension(1:MAXBOUND)::d_bound=0.0d0
  real(dp),dimension(1:MAXBOUND)::p_bound=0.0d0
  real(dp),dimension(1:MAXBOUND)::u_bound=0.0d0
  real(dp),dimension(1:MAXBOUND)::v_bound=0.0d0
  real(dp),dimension(1:MAXBOUND)::w_bound=0.0d0
  real(dp),dimension(1:MAXBOUND)::A_bound=0.0d0
  real(dp),dimension(1:MAXBOUND)::B_bound=0.0d0
  real(dp),dimension(1:MAXBOUND)::C_bound=0.0d0
#if NENER>0
  real(dp),dimension(1:MAXBOUND,1:NENER)::prad_bound=0.0
#endif
#if NVAR>8+NENER
  real(dp),dimension(1:MAXBOUND,1:NVAR-8-NENER)::var_bound=0.0
#endif

  ! Refinement parameters for hydro
  real(dp)::err_grad_d=-1.0  ! Density gradient
  real(dp)::err_grad_u=-1.0  ! Velocity gradient
  real(dp)::err_grad_p=-1.0  ! Pressure gradient
  real(dp)::err_grad_A=-1.0  ! Bx gradient
  real(dp)::err_grad_B=-1.0  ! By gradient
  real(dp)::err_grad_C=-1.0  ! Bz gradient
  real(dp)::err_grad_B2=-1.0 ! B L2 norm gradient
  real(dp)::floor_d=1.d-10   ! Density floor
  real(dp)::floor_u=1.d-10   ! Velocity floor
  real(dp)::floor_p=1.d-10   ! Pressure floor
  real(dp)::floor_A=1.d-10   ! Bx floor
  real(dp)::floor_B=1.d-10   ! By floor
  real(dp)::floor_C=1.d-10   ! Bz floor
  real(dp)::floor_b2=1.d-10  ! B L2 norm floor
  real(dp)::mass_sph=0.0D0   ! mass_sph
#if NENER>0
  real(dp),dimension(1:NENER)::err_grad_prad=-1.0
#endif
#if NVAR>8+NENER
  real(dp),dimension(1:NVAR-8-NENER)::err_grad_var=-1.0
#endif
  real(dp),dimension(1:MAXLEVEL)::jeans_refine=-1.0

  ! Initial conditions hydro variables
  integer::disk_setup=1

  integer ::disk_nsubsample=0
  real(dp)::disk_aspect=0.05d0
  real(dp)::disk_shear_forcing=1.d0
  real(dp)::disk_raiseP_factor=0.d0
  real(dp)::disk_testR=-1.d0
  real(dp)::disk_testz=-1.d0
  real(dp)::grav_angle=-1.d0
  real(dp)::grav_width=0.3d0
  real(dp)::disk_trans_fact=1.d0
  real(dp)::disk_exp_limit=20.0
  real(dp)::disk_xc=0.d0,disk_yc=0.d0,disk_zc=0.d0
  real(dp)::disk_soft=0.01d0
  real(dp)::disk_R0=5.0
  real(dp)::disk_R200=300.0
  real(dp)::disk_R1=6.0
  real(dp)::disk_H1=0.15
  real(dp)::disk_conc=13.0
  real(dp)::disk_innerR=0.5    ! Transition radius to inner boundary
  real(dp)::disk_outerR=1.d30  ! Transition radius to outer boundary
  real(dp)::disk_outerR2=19.8  ! Transition radius to second outer boundary, i.e., HTL17
  real(dp)::disk_innerR0=0.1   ! Transition to zero gravity in inner boundary
  real(dp)::disk_outerR0=1.d30 !  Transition to zero gravity in outer boundary
  real(dp)::disk_V0=2.0
  real(dp)::disk_vramp=1.0d0
  real(dp)::disk_vrms=0.0d0 ! < 0 means fraction of Cs, Gressel = -0.01
  real(dp)::disk_B0=0.0
  real(dp)::disk_By=0.0
  real(dp)::disk_Bz=1.d30
  real(dp)::disk_mag_pow=0.d0
  real(dp)::disk_mag_scaleBh=0.d0
  real(dp)::disk_mag_scaleBR=0.d0
  real(dp)::disk_mag_scaleH=1.d30
  real(dp)::disk_mag_scaleR=1.d30
  real(dp)::disk_D0=1.0
  real(dp)::disk_D1=1.0
  real(dp)::disk_P0=1.0
  real(dp)::disk_Mass=1.0
  real(dp)::disk_thetaMax=0.5236
  real(dp)::disk_Textra=1.05
  real(dp)::disk_ratio=0.05
  real(dp)::disk_T0=100.0
  real(dp)::disk_T1=1d10 ! max temp; < 0 means factor times boundary edge temperature
  real(dp)::disk_m=0.5
  real(dp)::disk_rho_pow=-1.5d0
  real(dp)::disk_cs_pow=-1.0d0
  real(dp)::disk_abar=1.22
  real(dp)::disk_delta_alpha=1.d0 ! what disk_aspect will vary to within innerR. 1 means unchanged.

! table for interpolating rotation velocity
  integer, parameter :: n_zint=2**20
  real(dp), dimension(n_zint) :: vrot_table = 0.d0, p_table, d_table, P0_table, P1_table, P2_table, P3_table
  logical :: disk_ReadTable=.true.
  logical :: disk_smooth_vertRho=.true.
  integer :: disk_TableLevel=-1

  real(dp),dimension(1:MAXREGION)::d_region=0.
  real(dp),dimension(1:MAXREGION)::u_region=0.
  real(dp),dimension(1:MAXREGION)::v_region=0.
  real(dp),dimension(1:MAXREGION)::w_region=0.
  real(dp),dimension(1:MAXREGION)::p_region=0.
  real(dp),dimension(1:MAXREGION)::A_region=0.
  real(dp),dimension(1:MAXREGION)::B_region=0.
  real(dp),dimension(1:MAXREGION)::C_region=0.
#if NENER>0
  real(dp),dimension(1:MAXREGION,1:NENER)::prad_region=0.0
#endif
#if NVAR>8+NENER
  real(dp),dimension(1:MAXREGION,1:NVAR-8-NENER)::var_region=0.0
#endif

  ! Hydro solver parameters
  integer ::niter_riemann=10
  integer ::slope_type=1
  integer ::slope_mag_type=-1
  real(dp)::slope_theta=1.5d0
  real(dp)::gamma=1.4d0
  real(dp),dimension(1:512)::gamma_rad=1.33333333334d0
  real(dp)::courant_factor=0.5d0
  logical::apply_relaxation=.false.
  logical::relaxation_unew_uold=.false.
  real(dp)::relaxation_time=0.d0
  real(dp)::relaxation_OuterRadius=0.d0 ! > 0 spherical, < 0 cylindrical
  real(dp)::relaxation_InnerRadius=0.d0 ! > 0 spherical, < 0 cylindrical
  real(dp)::relax_factor=2.d0
  real(dp)::relax_revert=1.d0
  integer ::relaxation_step=0
  real(dp)::difmag=0.0d0
  real(dp)::smallc=1.d-10
  real(dp)::smallr=1.d-10
  real(dp)::eta_mag=0.0d0
  character(LEN=10)::scheme='muscl'
  character(LEN=10)::riemann='llf'
  character(LEN=10)::riemann2d='llf'
  integer ::ischeme=0
  integer ::iriemann=0
  integer ::iriemann2d=0

  ! Interpolation parameters
  integer ::interpol_var=0
  integer ::interpol_type=1
  integer ::interpol_mag_type=-1

  ! Passive variables index
  integer::imetal=9
  integer::idelay=9
  integer::ixion=9
  integer::ichem=9
  integer::ivirial1=9
  integer::ivirial2=9
  integer::inener=9

end module hydro_parameters
