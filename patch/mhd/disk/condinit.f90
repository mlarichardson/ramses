!================================================================
!================================================================
!================================================================
!================================================================
subroutine condinit(x,u,dx,nn)
  use amr_parameters
  use hydro_parameters
  use poisson_parameters
  implicit none
  integer ::nn                            ! Number of cells
  real(dp)::dx                            ! Cell size
  real(dp),dimension(1:nvector,1:nvar+3)::u ! Conservative variables
  real(dp),dimension(1:nvector,1:ndim)::x ! Cell center position.
  !================================================================
  ! This routine generates initial conditions for RAMSES.
  ! Positions are in user units:
  ! x(i,1:3) are in [0,boxlen]**ndim.
  ! U is the conservative variable vector. Conventions are here:
  ! U(i,1): d, U(i,2:4): d.u,d.v,d.w, U(i,5): E, U(i,6:8): Bleft,
  ! U(i,nvar+1:nvar+3): Bright
  ! Q is the primitive variable vector. Conventions are here:
  ! Q(i,1): d, Q(i,2:4):u,v,w, Q(i,5): P, Q(i,6:8): Bleft,
  ! Q(i,nvar+1:nvar+3): Bright
  ! If nvar > 8, remaining variables (9:nvar) are treated as passive
  ! scalars in the hydro solver.
  ! U(:,:) and Q(:,:) are in user units.
  !================================================================

  !--------------------------------------------------
  ! Local variables
  !--------------------------------------------------
  real(dp)::scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2

#if NENER>0
  integer::irad
#endif
#if NVAR>8+NENER
  integer::ivar
#endif
  real(dp),dimension(1:nvector,1:nvar+3),save::q   ! Primitive variables
  real(dp)::xc,xr,xl,yl,yr,yc,zc,zr,zl,R0,Rin,Rout,V0,P0,pi,f,rr,abar,alpha,beta
  real(dp)::m,power,pm1,pp1,const1,const2,thetamax,GM,All,Alr,Arl,Arr
  real(dp)::theta,g,D0,D1,dens,T0,r_cyl,v_cyl,r_sph,dx_min,

  pi=ACOS(-1.0d0)
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Add here, if you wish, some user-defined initial conditions
  ! ........

! Gressel values ... simpler disk.  --> type 1
! gamma = 7./5.
! abar = 1.22 ?
! R0 = 5 AU
! T0 = 108 K
! Sigma = 150 g / cm^2 @ R0 ... not sure what this means for D0.
!  If I've calculated correctly, this gives D0 = Sigma / (2 * R0 * .0427878) ! I have double checked this!
!     .0427878 = integral of (exp(400(1/sqrt(1+x^2) - 1))) from 0 to .05.
!       --> D0 = 2.3434e-11 (only for unflared model, q = -1)
! theta_max = 0.4 radians above / below midplane. (only for unflared model, q = -1)
! R on = 0.5 - 5.5 AU
! rest is 1024 x 384
!  v_theta = sqrt(G Msun / R ) * r/R * sqrt( (p+q)/400 + (1+q) - qR/r)
!    p = -3/2   q = -1 , gives: (note in this code I use alpha and beta instead of p and q)
!     v_theta = sqrt(G Msun / R) * r/R * sqrt( max(R/r - 1/240, 0) )
!
!  beta_p0 = 2p/B^2 = 1e5
!   Bz = sqrt(beta_p0/2/p)
!

! Bai values for below: --> type 2
!   alpha = 2.0

!   D0 = 1
!   T0 = 1
!   GM = 1
!   m = 0.5
!   beta0 = 1e4 = q5/(B^2/8 pi) --> B0^2 + Bz^2 = ?

!  Geom is 2D spherical polar with
!   rmin = 1, rmax = 100, logarithmically spaced
!   thetamin = 2 degrees, max = 178 degrees, grid spacing larger at poles by factor 4 to midplane.

! Setting up an ideal MHD wedge version of Bai & Stone 2017
! Note, best way to get B field is use vector potential on
! corners and take first order finite difference. This well
! have left / right states be the same on neighbouring cells


! No B, Keplarian disk --> type 3
  ! Magnetic Potential terms
  Rin=disk_innerR
  Rout=disk_outerR
  R0=disk_R0

  alpha = disk_alpha
  beta  = disk_beta
  abar  = disk_abar
  power = (1.d0 - alpha)/2.d0
  pm1 = power - 1.d0
  pp1 = power + 1.d0
  m = disk_m

  B0 = disk_B0
  By = disk_By
  Bz = disk_Bz 

  V0 = disk_V0
  D0 = disk_D0
  D1 = disk_D1
  T0 = disk_T0
  GM = disk_Mass ! Msun
  GM = GM*6.67d-8*1.989d33 !
  thetaMax=disk_thetaMax

  const1 = GM
  ! Note: 149597871 = 1AU in km --> Below is for 5AU
  const2 = const1/(R0*149597871d0)

  if ( disk_setup == 1 ) then ! Gressel
    gravity_params(1) = GM*scale_t**2/scale_l**3 ! GM in code units
    gravity_params(2) = 7e10/scale_l ! softening
    ! grav_params(3:5) = position of sun = (0,0,0) = default grav_params value
    do i=1,nn
      xc=x(i,1) - boxlen/2.d0
      yc=x(i,2) - boxlen/2.d0
      zc=x(i,3) - boxlen/2.d0

      r_cyl=sqrt(xc**2+yc**2)
      r_sph=sqrt(xc**2+yc**2+zc**2)

      ! Some question about whether v_cyl should be R * Omega(R) , or r * Omega(R), or r * Omega(r). I'm using the last below, for now.
      v_cyl = sqrt(const1/(r_sph*scale_l)) * sqrt( max(0.d0,(alpha + beta)/400.d0 + (1+beta) - beta*r_cyl/r_sph))/(scale_l/scale_t)

      q(i,1)= D0 * (r_cyl/R0)**(alpha) * exp(400.d0*(r_cyl/r_sph - 1.d0)*(r_cyl/R0)**(-beta) ) /scale_d
      q(i,2)= -v_cyl*yc/r_cyl
      q(i,3)=  v_cyl*xc/r_cyl
      q(i,4)= 0.d0
      if (abs(zc) .gt. disk_ratio*Rout*2.d0) then
        r_sph=sqrt(xc**2+yc**2+(disk_ratio*Rout*2.d0)**2)
        dens = D0 * (r_cyl/R0)**(alpha) * exp(400.d0*(r_cyl/r_sph - 1.d0)*(r_cyl/R0)**(-beta) ) /scale_d
        q(i,5)= 8.314472d7 * T0 * (r_cyl/R0)**beta * dens / abar /(scale_l**2 / scale_t**2)
      else
        q(i,5)= 8.314472d7 * T0 * (r_cyl/R0)**beta * q(i,1) / abar /(scale_l**2 / scale_t**2)
      endif
      if ( r_cyl .lt. Rin .or. r_cyl .gt. Rout .or. abs(zc) .gt. disk_ratio*Rout ) q(i,1) = D1/scale_d

      ! Left B fields - Assuming By is beta for y.
      q(i,6)     = 0.d0
      q(i,7)     = 0.d0
      q(i,8)     = sqrt(2.d0 * 8.314472d7 * T0 * D0 / scale_d * (r_cyl/R0)**(alpha + beta) / abar /(scale_l**2 / scale_t**2) / By )

      ! Right B fields. Div * B is zero with linear operator (Bxr - Bxl)/dx ...
      q(i,nvar+1)= 0.d0
      q(i,nvar+2)= 0.d0
      q(i,nvar+3)= sqrt(2.d0 * 8.314472d7 * T0 * D0 / scale_d * (r_cyl/R0)**(alpha + beta) / abar /(scale_l**2 / scale_t**2 / By )

    enddo
  elseif ( disk_setup == 2 ) then ! Bai

    do i=1,nn

      xl=x(i,1)-0.5*dx
      xr=x(i,1)+0.5*dx
      xc=x(i,1)
      yl=x(i,2)-0.5*dx
      yr=x(i,2)+0.5*dx
      yc=x(i,2)

      All = B0/pp1 * (xl/R0)**pm1*(1.d0 + (yl/m/xl)**2)**(0.625)*xl
      Arl = B0/pp1 * (xr/R0)**pm1*(1.d0 + (yl/m/xr)**2)**(0.625)*xr
      Alr = B0/pp1 * (xl/R0)**pm1*(1.d0 + (yr/m/xl)**2)**(0.625)*xl
      Arr = B0/pp1 * (xr/R0)**pm1*(1.d0 + (yr/m/xr)**2)**(0.625)*xr

      rr=sqrt(xc**2+yc**2)

      q(i,1)= D0 * (rr/R0)**(-alpha) * f(theta)
      q(i,2)= 0.
      q(i,3)= 0.
      q(i,4)= ( GM - (alpha + 1) * T0 * R0 * g(theta) ) / rr
      q(i,5)= T0 * (R0/rr) * g(theta) * q(i,1)

      ! Left B fields
      q(i,6)     = (Alr - All)/dx
      q(i,7)     =-(Arl - All)/dx
      q(i,8)     =      Bz

      ! Right B fields. Div * B is zero with linear operator (Bxr - Bxl)/dx ...
      q(i,nvar+1)= (Arr - Arl)/dx
      q(i,nvar+2)=-(Arr - Alr)/dx
      q(i,nvar+3)=      Bz

    end do
  else if ( disk_setup == 3 ) then ! NoB Kepler ring
    gravity_params(1) = GM*scale_t**2/scale_l**3 ! GM in code units
    gravity_params(2) = 7e10/scale_l ! softening
    ! grav_params(3:5) = position of sun = (0,0,0) = default grav_params value
    do i=1,nn
      xc=x(i,1)
      yc=x(i,2)

      rr=sqrt((xc-R0)**2+yc**2)

      if (rr < Rout) then
        q(i,1)= D0/scale_d
      else
        q(i,1)= 1d-16/scale_d
      endif
      q(i,2)= 0.
      q(i,3)= 0.
      ! Some question about whether v_z should be R * Omega , or r * Omega. I'm using the first below, but multiply by rr/xc if you change
      q(i,4)= sqrt(const1/(xc*scale_l))*scale_t/scale_l
      q(i,5)= 8.314472d7 * T0 * D0 / abar /(scale_l**2 / scale_t**2)

      ! Left B fields - Assuming By is beta for y.
      q(i,6)     = 0.d0
      q(i,7)     = 0.d0
      q(i,8)     = B0

      ! Right B fields. Div * B is zero with linear operator (Bxr - Bxl)/dx ...
      q(i,nvar+1)= 0.d0
      q(i,nvar+2)= 0.d0
      q(i,nvar+3)= B0

    enddo
  else if ( disk_setup == 4 ) then ! Weird ramp for tesdting
    gravity_params(1) = GM*scale_t**2/scale_l**3 ! GM in code units
    gravity_params(2) = 7e10/scale_l ! softening
    ! grav_params(3:5) = position of sun = (0,0,0) = default grav_params value
    do i=1,nn
      xc=x(i,1)
      yc=x(i,2)

      q(i,1)= D0*(1.d0 + x(i,1)/boxlen)/scale_d
      q(i,2)= 0.
      q(i,3)= 0.
      q(i,4)= V0
      q(i,5)= 8.314472d7 * T0 * D0 / abar /(scale_l**2 / scale_t**2)

      ! Left B fields - Assuming By is beta for y.
      q(i,6)     = 0.d0
      q(i,7)     = 0.d0
      q(i,8)     = B0

      ! Right B fields. Div * B is zero with linear operator (Bxr - Bxl)/dx ...
      q(i,nvar+1)= 0.d0
      q(i,nvar+2)= 0.d0
      q(i,nvar+3)= B0

    enddo

  else
    stop "change rotor_setup to be 1 or 2"
  endif


  ! Convert primitive to conservative variables
  ! density -> density
  u(1:nn,1)=q(1:nn,1)
  ! velocity -> momentum
  u(1:nn,2)=q(1:nn,1)*q(1:nn,2)
  u(1:nn,3)=q(1:nn,1)*q(1:nn,3)
  u(1:nn,4)=q(1:nn,1)*q(1:nn,4)
  ! kinetic energy
  u(1:nn,5)=0.0d0
  u(1:nn,5)=u(1:nn,5)+0.5*q(1:nn,1)*q(1:nn,2)**2
  u(1:nn,5)=u(1:nn,5)+0.5*q(1:nn,1)*q(1:nn,3)**2
  u(1:nn,5)=u(1:nn,5)+0.5*q(1:nn,1)*q(1:nn,4)**2
  ! pressure -> total fluid energy
  u(1:nn,5)=u(1:nn,5)+q(1:nn,5)/(gamma-1.0d0)
  ! magnetic energy -> total fluid energy
  u(1:nn,5)=u(1:nn,5)+0.125d0*(q(1:nn,6)+q(1:nn,nvar+1))**2
  u(1:nn,5)=u(1:nn,5)+0.125d0*(q(1:nn,7)+q(1:nn,nvar+2))**2
  u(1:nn,5)=u(1:nn,5)+0.125d0*(q(1:nn,8)+q(1:nn,nvar+3))**2
  u(1:nn,6:8)=q(1:nn,6:8)
  u(1:nn,nvar+1:nvar+3)=q(1:nn,nvar+1:nvar+3)
#if NENER>0
  ! radiative pressure -> radiative energy
  ! radiative energy -> total fluid energy
  do irad=1,nener
     u(1:nn,8+irad)=q(1:nn,8+irad)/(gamma_rad(irad)-1.0d0)
     u(1:nn,5)=u(1:nn,5)+u(1:nn,8+irad)
  enddo
#endif
#if NVAR>8+NENER
  ! passive scalars
  do ivar=9+nener,nvar
     u(1:nn,ivar)=q(1:nn,1)*q(1:nn,ivar)
  end do
#endif

end subroutine condinit
!================================================================
!================================================================
!================================================================
!================================================================
subroutine velana(x,v,dx,t,ncell)
  use amr_parameters
  use hydro_parameters
  implicit none
  integer ::ncell                         ! Size of input arrays
  real(dp)::dx                            ! Cell size
  real(dp)::t                             ! Current time
  real(dp),dimension(1:nvector,1:3)::v    ! Velocity field
  real(dp),dimension(1:nvector,1:ndim)::x ! Cell center position.
  !================================================================
  ! This routine computes the user defined velocity fields.
  ! x(i,1:ndim) are cell center position in [0,boxlen] (user units).
  ! v(i,1:3) is the imposed 3-velocity in user units.
  !================================================================
  integer::i
  real(dp)::xx,yy=0.,zz=0.,vx,vy,vz,aa,twopi
!!$  real(dp)::rr,tt,omega
  
  ! Add here, if you wish, some user-defined initial conditions
  aa=1.0+0.*t
  twopi=2d0*ACOS(-1d0)
  do i=1,ncell

     xx=x(i,1)+0.*dx
#if NDIM > 1
     yy=x(i,2)
#endif
#if NDIM > 2
     zz=x(i,3)
#endif
     ! ABC
     vx=aa*(cos(twopi*yy)+sin(twopi*zz))
     vy=aa*(sin(twopi*xx)+cos(twopi*zz))
     vz=aa*(cos(twopi*xx)+sin(twopi*yy))

!!$     ! 1D advection test
!!$     vx=1.0_dp
!!$     vy=0.0_dp
!!$     vz=0.0_dp

!!$     ! Ponomarenko
!!$     xx=xx-boxlen/2.0
!!$     yy=yy-boxlen/2.0
!!$     rr=sqrt(xx**2+yy**2)
!!$     if(yy>0)then
!!$        tt=acos(xx/rr)
!!$     else
!!$        tt=-acos(xx/rr)+twopi
!!$     endif
!!$     if(rr<1.0)then
!!$        omega=0.609711
!!$        vz=0.792624
!!$     else
!!$        omega=0.0
!!$        vz=0.0
!!$     endif
!!$     vx=-sin(tt)*rr*omega
!!$     vy=+cos(tt)*rr*omega

     v(i,1)=vx
#if NDIM > 1
     v(i,2)=vy
#endif
#if NDIM > 2
     v(i,3)=vz
#endif
  end do


end subroutine velana

subroutine mag_compute(ilevel)
  use amr_commons
  !use pm_commons
  use hydro_commons
  !use random
  implicit none
  integer::i,j,ilevel,icell
  logical::nogal
  real(dp)::Axdl,Axdr,Axul,Axur
  real(dp)::Aydl,Aydr,Ayul,Ayur
  real(dp)::Azdl,Azdr,Azul,Azur
  real(dp)::Bxl,Bxr,Byl,Byr,Bzl,Bzr
  real(dp),dimension(1:3)::pos,cell_center

  real(dp)::dx,dxhalf,dxmin,dxminhalf,scale,dx_loc,vol_loc
  integer::nx_loc,ind,ix,iy,iz,iskip,nfine
  real(dp),dimension(1:3)::skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc

  ! Mesh spacing in that level
  dx=0.5D0**ilevel
  dxhalf = 0.5D0*dx
  dxmin = 0.5D0**nlevelmax
  dxminhalf = 0.5D0*dxmin
  nfine = 2**(nlevelmax-ilevel)

  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**ndim
  do ind=1,twotondim
     iz=(ind-1)/4
     iy=(ind-1-4*iz)/2
     ix=(ind-1-2*iy-4*iz)
     if(ndim>0)xc(ind,1)=(dble(ix)-0.5D0)*dx
     if(ndim>1)xc(ind,2)=(dble(iy)-0.5D0)*dx
     if(ndim>2)xc(ind,3)=(dble(iz)-0.5D0)*dx
  end do

  ! compute field
  do ind=1,twotondim
    iskip=ncoarse+(ind-1)*ngridmax
    do j=1,active(ilevel)%ngrid
      icell=active(ilevel)%igrid(j)
      cell_center = xg(icell,:)+xc(ind,:)-skip_loc(:)

      ! edge coordinates
      ! Ax
      pos = cell_center
      pos(1) = pos(1) - dxhalf + dxminhalf
      pos(2) = pos(2) - dxhalf
      pos(3) = pos(3) - dxhalf
      Axdl=0.0;Axdr=0.0;Axul=0.0;Axur=0.0
      do i=1,nfine
        CALL mag_potential(pos,1,Axdl)
        pos(1) = pos(1) + dxmin
      enddo
      pos(2) = pos(2) + dx
      do i=1,nfine
        pos(1) = pos(1) - dxmin
        CALL mag_potential(pos,1,Axdr)
      enddo
      pos(3) = pos(3) + dx
      do i=1,nfine
        CALL mag_potential(pos,1,Axur)
        pos(1) = pos(1) + dxmin
      enddo
      pos(2) = pos(2) - dx
      do i=1,nfine
        pos(1) = pos(1) - dxmin
        CALL mag_potential(pos,1,Axul)
      enddo
      ! Ay
      pos = cell_center
      pos(1) = pos(1) - dxhalf 
      pos(2) = pos(2) - dxhalf + dxminhalf
      pos(3) = pos(3) - dxhalf
      Aydl=0.0;Aydr=0.0;Ayul=0.0;Ayur=0.0
      do i=1,nfine
        CALL mag_potential(pos,2,Aydl)
        pos(2) = pos(2) + dxmin
      enddo
      pos(1) = pos(1) + dx
      do i=1,nfine
        pos(2) = pos(2) - dxmin
        CALL mag_potential(pos,2,Aydr)
      enddo
      pos(3) = pos(3) + dx
      do i=1,nfine
        CALL mag_potential(pos,2,Ayur)
        pos(2) = pos(2) + dxmin
      enddo
      pos(1) = pos(1) - dx
      do i=1,nfine
        pos(2) = pos(2) - dxmin
        CALL mag_potential(pos,2,Ayul)
      enddo
      ! Az
      pos = cell_center
      pos(1) = pos(1) - dxhalf
      pos(2) = pos(2) - dxhalf
      pos(3) = pos(3) - dxhalf + dxminhalf
      Azdl=0.0;Azdr=0.0;Azul=0.0;Azur=0.0
      do i=1,nfine
        CALL mag_potential(pos,3,Azdl)
        pos(3) = pos(3) + dxmin
      enddo
      pos(1) = pos(1) + dx
      do i=1,nfine
        pos(3) = pos(3) - dxmin
        CALL mag_potential(pos,3,Azdr)
      enddo
      pos(2) = pos(2) + dx
      do i=1,nfine
        CALL mag_potential(pos,3,Azur)
        pos(3) = pos(3) + dxmin
      enddo
      pos(1) = pos(1) - dx
      do i=1,nfine
        pos(3) = pos(3) - dxmin
        CALL mag_potential(pos,3,Azul)
      enddo

      ! Bx left
      Bxl = ((Azul - Azdl) - (Ayul - Aydl))/dx / nfine
      uold(icell+iskip,6)      = uold(icell+iskip,6) + Bxl
      ! By left
      Byl = ((Axul - Axdl) - (Azdr - Azdl))/dx / nfine
      uold(icell+iskip,7)      = uold(icell+iskip,7) + Byl
      ! Bz left
      Bzl = ((Aydr - Aydl) - (Axdr - Axdl))/dx / nfine
      uold(icell+iskip,8)      = uold(icell+iskip,8) + Bzl

      ! Bx right
      Bxr = ((Azur - Azdr) - (Ayur - Aydr))/dx / nfine
      uold(icell+iskip,nvar+1) = uold(icell+iskip,nvar+1) + Bxr
      ! By right
      Byr = ((Axur - Axdr) - (Azur - Azul))/dx / nfine
      uold(icell+iskip,nvar+2) = uold(icell+iskip,nvar+2) + Byr
      ! Bz right
      Bzr = ((Ayur - Ayul) - (Axur - Axul))/dx / nfine
      uold(icell+iskip,nvar+3) = uold(icell+iskip,nvar+3) + Bzr
    end do
  end do

end subroutine mag_compute

subroutine mag_potential(pos,dir,A)
  use amr_parameters, ONLY: boxlen
  implicit none
  real(dp)::r,h
  real(dp)::Ah,A,cross,Bpow,AR
  integer::i,dir
  real(dp),dimension(1:3)::pos,gcenter,gaxis,xrel,grad
  real(dp)::sBR,sBh,sR,sH

  ! Note : toroidal field comes from a Bz term alone, particular one with 
  !   Az ~ f(z) * exp(-R/h) --> Bx/y = -By/x = Az/h/R 

  ! Note : vertical field comes from a B_R term alone, where Ax/y = - Ay/x
  !   A ~ A0 / R^m (-y/R,x/R,0) --> Bz = A0 / R^(m+1) *(1-m)
 
  gcenter(1) = 0.5d0 ! disk_cenX
  gcenter(2) = 0.5d0 ! disk_cenY
  gcenter(3) = 0.5d0 ! disk_cenZ

  gaxis(1) = 0.d0 ! disk_axisX
  gaxis(2) = 0.d0 ! disk_axisY
  gaxis(3) = 1.d0 ! disk_axisZ

  sR = disk_mag_scaleR / boxlen
  sH = disk_mag_scaleH / boxlen
  sBh = disk_mag_scaleBh
  sBr = disk_mag_scaleBR

  Bpow = disk_mag_pow

  ! coordinates in galaxy frame
  xrel = pos - gcenter
  h = DOT_PRODUCT(xrel,gaxis)
  grad = xrel - h*gaxis
  r = NORM2(grad)

  cross = CROSS_PRODUCT(gaxis,grel)
  ! toroidal field
  Ah = sBh * sR * exp(-r/sR) * exp(-ABS(h)/sH)
  
  ! vertical field
  AR = sBr /r**(Bpow+1)

  ! vector in cartesian frame
  A = A + Ah*gaxis(dir) + AR*cross(dir)

end subroutine mag_potential

