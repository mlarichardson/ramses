!================================================================
!================================================================
!================================================================
!================================================================
subroutine condinit(x,u,dx,nn)
  use amr_parameters
  use hydro_parameters
  use poisson_parameters
  implicit none
  integer ::nn                            ! Number of cells
  real(dp)::dx                            ! Cell size
  real(dp),dimension(1:nvector,1:nvar+3)::u ! Conservative variables
  real(dp),dimension(1:nvector,1:ndim)::x ! Cell center position.

  !--------------------------------------------------
  ! Local variables
  !--------------------------------------------------
  real(dp)::scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2

  !================================================================
  ! This routine generates initial conditions for RAMSES.
  ! Positions are in user units:
  ! x(i,1:3) are in [0,boxlen]**ndim.
  ! U is the conservative variable vector. Conventions are here:
  ! U(i,1): d, U(i,2:4): d.u,d.v,d.w, U(i,5): E, U(i,6:8): Bleft,
  ! U(i,nvar+1:nvar+3): Bright
  ! Q is the primitive variable vector. Conventions are here:
  ! Q(i,1): d, Q(i,2:4):u,v,w, Q(i,5): P, Q(i,6:8): Bleft,
  ! Q(i,nvar+1:nvar+3): Bright
  ! If nvar > 8, remaining variables (9:nvar) are treated as passive
  ! scalars in the hydro solver.
  ! U(:,:) and Q(:,:) are in user units.
  !================================================================
  integer::ivar,i
  real(dp),dimension(1:nvector,1:nvar+3),save::q   ! Primitive variables
  real(dp)::xc,xr,xl,yl,yr,yc,R0,R1,V0,B0,P0,pi,f,rr,abar,By,Bz,alpha,beta
  real(dp)::m,power,pm1,pp1,const1,const2,thetamax,GM,All,Alr,Arl,Arr
  real(dp)::theta,g,D0,T0
 
  pi=ACOS(-1.0d0)
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

! Gressel values ... simpler disk.  --> type 1
! gamma = 7./5.
! abar = 1.22 ? 
! R0 = 5 AU
! T0 = 108 K
! Sigma = 150 g / cm^2 @ R0 ... not sure what this means for D0. 
!  If I've calculated correctly, this gives D0 = Sigma / (2 * R0 * .0427878) ! I have double checked this! 
!     .0427878 = integral of (exp(400(1/sqrt(1+x^2) - 1))) from 0 to .05. 
!       --> D0 = 2.3434e-11 (only for unflared model, q = -1)
! theta_max = 0.4 radians above / below midplane. (only for unflared model, q = -1)
! R on = 0.5 - 5.5 AU 
! rest is 1024 x 384 
!  v_theta = sqrt(G Msun / R ) * r/R * sqrt( (p+q)/400 + (1+q) - qR/r) 
!    p = -3/2   q = -1 , gives: (note in this code I use alpha and beta instead of p and q)
!     v_theta = sqrt(G Msun / R) * r/R * sqrt( max(R/r - 1/240, 0) )
!
!  beta_p0 = 2p/B^2 = 1e5
!   Bz = sqrt(beta_p0/2/p)
! 

! Bai values for below: --> type 2
!   alpha = 2.0
!   D0 = 1
!   T0 = 1
!   GM = 1
!   m = 0.5
!   beta0 = 1e4 = q5/(B^2/8 pi) --> B0^2 + Bz^2 = ? 

!  Geom is 2D spherical polar with
!   rmin = 1, rmax = 100, logarithmically spaced
!   thetamin = 2 degrees, max = 178 degrees, grid spacing larger at poles by factor 4 to midplane.
 
! Setting up an ideal MHD wedge version of Bai & Stone 2017
! Note, best way to get B field is use vector potential on 
! corners and take first order finite difference. This well 
! have left / right states be the same on neighbouring cells


! No B, Keplarian disk --> type 3
  ! Magnetic Potential terms
  B0=rotor_B0
  R0=rotor_R0
  R1=rotor_R1
  By=rotor_By
  Bz=rotor_Bz
  alpha = rotor_alpha
  beta  = rotor_beta
  abar  = rotor_abar
  power = (1.d0 - alpha)/2.d0 
  pm1 = power - 1.d0
  pp1 = power + 1.d0
  m = rotor_m 
  
  D0 = rotor_D0
  T0 = rotor_T0
  GM = rotor_Mass ! Msun
  GM = GM*6.67d-8*1.989d33 ! 
  thetaMax=rotor_thetaMax

  const1 = GM  
  ! Note: 149597871 = 1AU in km --> Below is for 5AU 
  const2 = const1/(R0*149597871d0)

  if ( rotor_setup == 1 ) then ! Gressel
    gravity_params(1) = GM*scale_t**2/scale_l**3 ! GM in code units
    gravity_params(2) = 7e10/scale_l ! softening
    ! grav_params(3:5) = position of sun = (0,0,0) = default grav_params value
    if (rotor_omega .lt. 0 .and. geom .eq. 2 .and. ndim .eq. 2) then
      rotor_omega = sqrt(const1/(R0*scale_l)) * sqrt( max(0.d0,(alpha + beta)/400.d0 + 1.d0))/R0 * scale_t/scale_l
    endif
    do i=1,nn
      xc=x(i,1) + rotor_X0
      yc=x(i,2) + rotor_Y0 

      rr=sqrt(xc**2+yc**2)

      q(i,1)= D0 * (xc/R0)**(alpha) * exp(400.d0*(xc/rr - 1.d0)*(xc/R0)**(-beta) ) /scale_d
      q(i,2)= 0.
      q(i,3)= 0.
      ! Some question about whether v_z should be R * Omega , or r * Omega. I'm using the first below, but multiply by rr/xc if you change 
      q(i,4)= sqrt(const1/(xc*scale_l)) * sqrt( max(0.d0,(alpha + beta)/400.d0 + (1+beta) - beta*xc/rr))/(scale_l/scale_t) - xc*rotor_omega
      q(i,5)= 8.314472d7 * T0 * (xc/R0)**beta * q(i,1) / abar /(scale_l**2 / scale_t**2)

      ! Left B fields - Assuming By is beta for y. 
      q(i,6)     = 0.d0 
      q(i,7)     = sqrt(2.d0 * 8.314472d7 * T0 * D0 / scale_d * (xc/R0)**(alpha + beta) / abar /(scale_l**2 / scale_t**2) / By )
      q(i,8)     = 0.d0

      ! Right B fields. Div * B is zero with linear operator (Bxr - Bxl)/dx ...
      q(i,nvar+1)= 0.d0 
      q(i,nvar+2)= sqrt(2.d0 * 8.314472d7 * T0 * D0 / scale_d * (xc/R0)**(alpha + beta) / abar /(scale_l**2 / scale_t**2) / By )
      q(i,nvar+3)= 0.d0

    enddo
  elseif ( rotor_setup == 2 ) then ! Bai

    do i=1,nn

      xl=x(i,1)-0.5*dx
      xr=x(i,1)+0.5*dx
      xc=x(i,1)
      yl=x(i,2)-0.5*dx
      yr=x(i,2)+0.5*dx
      yc=x(i,2)

      All = B0/pp1 * (xl/R0)**pm1*(1.d0 + (yl/m/xl)**2)**(0.625)*xl
      Arl = B0/pp1 * (xr/R0)**pm1*(1.d0 + (yl/m/xr)**2)**(0.625)*xr
      Alr = B0/pp1 * (xl/R0)**pm1*(1.d0 + (yr/m/xl)**2)**(0.625)*xl
      Arr = B0/pp1 * (xr/R0)**pm1*(1.d0 + (yr/m/xr)**2)**(0.625)*xr

      rr=sqrt(xc**2+yc**2)

      q(i,1)= D0 * (rr/R0)**(-alpha) * f(theta)
      q(i,2)= 0.
      q(i,3)= 0.
      q(i,4)= ( GM - (alpha + 1) * T0 * R0 * g(theta) ) / rr
      q(i,5)= T0 * (R0/rr) * g(theta) * q(i,1)

      ! Left B fields
      q(i,6)     = (Alr - All)/dx 
      q(i,7)     =-(Arl - All)/dx
      q(i,8)     =      Bz

      ! Right B fields. Div * B is zero with linear operator (Bxr - Bxl)/dx ...
      q(i,nvar+1)= (Arr - Arl)/dx
      q(i,nvar+2)=-(Arr - Alr)/dx
      q(i,nvar+3)=      Bz

    end do
  else if ( rotor_setup == 3 ) then ! NoB Kepler ring
    gravity_params(1) = GM*scale_t**2/scale_l**3 ! GM in code units
    gravity_params(2) = 7e10/scale_l ! softening
    ! grav_params(3:5) = position of sun = (0,0,0) = default grav_params value
    if (rotor_omega .lt. 0 .and. geom .eq. 2 .and. ndim .eq. 2) then
      rotor_omega = sqrt(const1/(R0*scale_l)) / R0 * scale_t/scale_l
    endif
    do i=1,nn
      xc=x(i,1) + rotor_X0
      yc=x(i,2) + rotor_Y0 

      rr=sqrt((xc-R0)**2+yc**2)

      if (rr < R1) then
        q(i,1)= D0/scale_d
      else
        q(i,1)= 1e-20/scale_d
      endif
      q(i,2)= 0.
      q(i,3)= 0.
      ! Some question about whether v_z should be R * Omega , or r * Omega. I'm using the first below, but multiply by rr/xc if you change 
      q(i,4)= sqrt(const1/(xc*scale_l))*scale_t/scale_l - xc*rotor_omega
      q(i,5)= 8.314472d7 * T0 * D0 / abar /(scale_l**2 / scale_t**2)

      ! Left B fields - Assuming By is beta for y. 
      q(i,6)     = 0.d0 
      q(i,7)     = 0.d0
      q(i,8)     = B0

      ! Right B fields. Div * B is zero with linear operator (Bxr - Bxl)/dx ...
      q(i,nvar+1)= 0.d0 
      q(i,nvar+2)= 0.d0
      q(i,nvar+3)= B0

    enddo
  else
    stop "change rotor_setup to be 1 or 2"
  endif

  ! Convert primitive to conservative variables
  ! density -> density
  u(1:nn,1)=q(1:nn,1)
  ! velocity -> momentum
  u(1:nn,2)=q(1:nn,1)*q(1:nn,2)
  u(1:nn,3)=q(1:nn,1)*q(1:nn,3)
  u(1:nn,4)=q(1:nn,1)*q(1:nn,4)
  ! kinetic energy
  u(1:nn,5)=0.0d0
  u(1:nn,5)=u(1:nn,5)+0.5*q(1:nn,1)*q(1:nn,2)**2
  u(1:nn,5)=u(1:nn,5)+0.5*q(1:nn,1)*q(1:nn,3)**2
  u(1:nn,5)=u(1:nn,5)+0.5*q(1:nn,1)*q(1:nn,4)**2
  ! pressure -> total fluid energy
  u(1:nn,5)=u(1:nn,5)+q(1:nn,5)/(gamma-1.0d0)
  ! magnetic energy -> total fluid energy
  u(1:nn,5)=u(1:nn,5)+0.125d0*(q(1:nn,6)+q(1:nn,nvar+1))**2
  u(1:nn,5)=u(1:nn,5)+0.125d0*(q(1:nn,7)+q(1:nn,nvar+2))**2
  u(1:nn,5)=u(1:nn,5)+0.125d0*(q(1:nn,8)+q(1:nn,nvar+3))**2
  u(1:nn,6:8)=q(1:nn,6:8)
  u(1:nn,nvar+1:nvar+3)=q(1:nn,nvar+1:nvar+3)
  ! passive scalars
  do ivar=9,nvar
     u(1:nn,ivar)=q(1:nn,1)*q(1:nn,ivar)
  end do

end subroutine condinit
!================================================================
!================================================================
!================================================================
!================================================================
subroutine velana(x,v,dx,t,ncell)
  use amr_parameters
  use hydro_parameters
  implicit none
  integer ::ncell                         ! Size of input arrays
  real(dp)::dx                            ! Cell size
  real(dp)::t                             ! Current time
  real(dp),dimension(1:nvector,1:3)::v    ! Velocity field
  real(dp),dimension(1:nvector,1:ndim)::x ! Cell center position.
  !================================================================
  ! This routine computes the user defined velocity fields.
  ! x(i,1:ndim) are cell center position in [0,boxlen] (user units).
  ! v(i,1:3) is the imposed 3-velocity in user units.
  !================================================================
  integer::i
  real(dp)::xx,yy,zz,vx,vy,vz,rr,tt,omega,aa,twopi

  ! Add here, if you wish, some user-defined initial conditions
  aa=1.0
  twopi=2d0*ACOS(-1d0)
  do i=1,ncell

     xx=x(i,1)
     yy=x(i,2)
     zz=x(i,3)

     ! ABC
     vx=aa*(cos(twopi*yy)+sin(twopi*zz))
     vy=aa*(sin(twopi*xx)+cos(twopi*zz))
     vz=aa*(cos(twopi*xx)+sin(twopi*yy))

     v(i,1)=vx
     v(i,2)=vy
     v(i,3)=vz

  end do


end subroutine velana



function f(theta)

  implicit none

  double precision :: f, theta

  return

end function f



function g(theta)

  implicit none

  double precision :: g, theta
  double precision :: del_mid, del_cor, theta_trans, del, pi

  pi=ACOS(-1.0d0)

  del_mid = 0.1d0
  del_cor = 0.3d0
  theta_trans = 0.3d0

  if (theta .lt. pi/2.d0) then
    del = (pi/2. - theta_trans) - theta
  else
    del = theta - (pi/2. + theta_trans)
  endif

  g = del/del_mid
  g = (exp(2*g) - 1) / (exp(2*g) + 1)
  g = 0.5d0*(g + 1.d0) 
  g = g * ( del_cor - del_mid + (0.5 - del_cor)*max(del,0.d0)/(pi/2. - theta_trans) )
  g = (del_mid + g)**2

  return
end function g
