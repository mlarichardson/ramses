program GeneralRho

  implicit none

  ! Parameters
  double precision, parameter :: R0 = 5.d0 ! Characteristic Radius
  double precision, parameter :: Rg = 0.5d0 ! Boundary Radius
  double precision, parameter :: D0 = 1.5955d-11 ! Characteristic Density
  double precision, parameter :: D1 = 2.1842d-18 ! Minimum Density
  double precision, parameter :: T1 = 2160.d0 ! Maximum Temperature
  double precision, parameter :: p = -1.5d0 ! Rho power index
  double precision, parameter :: q = -1.0d0 ! cs power index
  double precision, parameter :: aspect = 0.05d0 ! aspect ratio H/R
  double precision, parameter :: delta_a = 1.0d0 ! Transition to central value
  double precision, parameter :: epsilon_p = 0.0d0 ! Add extra pressure in center? 
  double precision, parameter :: abar = 2.024d0 ! average mean molar mass

  ! Grid range and resolution
  double precision, parameter :: dx = 0.001
  double precision, parameter :: Rmax = 1.d0
  double precision, parameter :: zmax = 0.4d0


  ! Variables
  integer :: nR, nz, iR, iz, iR0, iz0
  double precision :: R1, r_sph, r_sph1, r_sph_cen, rho, rho_g, pres_g, R,  &
       z, A, B, C, rho2,rho2_0,pres2_0,exp_fact,z_exp_fact,alpha
  double precision :: Mass_of_R, rho_of_R, pres_of_R, P_const, temp,temp2, pres, pres2,alpha2,P_const2
  external :: Mass_of_R, rho_of_R, pres_of_R

  alpha = 1.d0 / aspect

  nR = Rmax / dx
  nz = zmax / dx
  P_const = 6.67d-8*1.989d33/alpha**2

  open(42, file='Rho_Pres_26001.csv', status='unknown')
  write(42,*) "R,z,rho,rho2,pres,pres2,temp,temp2"
  do iR=1,nR
    R = (iR-0.5d0)*dx

    do iz=1,nz
      z = (iz-0.5d0)*dx
      r_sph = sqrt(R**2 + z**2)


      rho = max(D0 * (R/R0)**p * exp( -alpha**2 * (1.d0 - R/r_sph) * (R/R0)**(-1-q)), D1)
      pres = rho * P_const * (R*1.496d13)**q
      temp = pres*abar/rho/8.314e7 
      rho = log10(rho)
      pres = log10(pres)
      temp = log10(temp)

      if (R .lt. Rg) then
        
        alpha2 = alpha*(1 - (1.d0 - delta_a)*(1.d0 - R/Rg)**3)
        P_const2 = 6.67d-8*1.989d33*Mass_of_R(r_sph/Rg)/alpha2**2

        ! Get rho & pres on the boundary in the midplane
        rho_g = D0 * (Rg/R0)**p
        pres_g = rho_g * P_const * (Rg*1.496d13)**q

        ! Get rho & pres in the midplane at R
        rho2_0 = rho_g * Rho_of_R(R/Rg) * Mass_of_R(R/Rg)/Mass_of_R(r_sph/Rg)
        pres2_0 = rho2_0 * P_const2 * (R*1.496d13)**q

        ! Vertical dependence
        exp_fact = -alpha2**2 * (1.d0 - R/R_sph) * (R/R0)**(-q-1.d0)
        rho2 = max(rho2_0*exp(exp_fact),D1)

        ! Need vertical gradient to scale as rho/R, but can add radial constant 
        pres2 = rho2 * P_const2 * (R*1.496d13)**q + epsilon_p*(pres_g * pres_of_R(R/Rg) - pres2_0)
        temp2 = pres2*abar/rho2/8.314e7 
        if (temp2 .gt. T1) then
          pres2 = pres2 * T1/temp2
          temp2 = T1
        endif
        rho2 = log10(rho2)
        pres2 = log10(pres2)
        temp2 = log10(temp2)
      else
        rho2 = rho
        pres2 = pres
        temp2 = temp
      endif

      write(42,'(es14.6,7(",",es14.6))') R, z, rho, rho2, pres, pres2, temp, temp2
    enddo
  enddo


  close(42)

end program GeneralRho

function Mass_of_r(x)
  implicit none
  double precision :: x, Mass_of_r

  if (x .lt. 1.d0) then
    Mass_of_r = x * ( (99.d0/8.d0)*x**3 - (154.d0/8.d0)*x**4  + (63.d0/8.d0)*x**5 )**2
  else
    Mass_of_r = 1.d0
  endif

  return
end function Mass_of_r

function Rho_of_r(x)
  implicit none
  double precision :: x, Rho_of_r

  ! Assumes constant 1st, 2nd, 3rd derivatives, delta = 2, alpha = -1.5
  if (x .lt. 1.d0) then
    Rho_of_r = 2.d0 - (31.d0*x**4 + 69.d0*x**5 - 143.d0*x**6 + 59.d0*x**7)/16.d0
  else
    Rho_of_r = 1.d0
  endif

  return
end function Rho_of_r

function Pres_of_r(x)
  implicit none
  double precision :: x, Pres_of_r

  ! Assumes constant 1st, 2nd, 3rd derivatives, delta = 3, alpha = -2.5
  if (x .lt. 1.d0) then
    Pres_of_r = 3.d0 - (275.d0*x**4 + 261.d0*x**5 - 795.d0*x**6 + 355.d0*x**7)/48.d0
  else
    Pres_of_r = 1.d0
  endif

  return
end function Pres_of_r
