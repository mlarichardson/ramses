!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine gravana(x,f,dx,ncell)
  use amr_parameters
  use poisson_parameters
  implicit none
  integer ::ncell                         ! Size of input arrays
  real(dp)::dx                            ! Cell size
  real(dp),dimension(1:nvector,1:ndim)::f ! Gravitational acceleration
  real(dp),dimension(1:nvector,1:ndim)::x ! Cell center position.
  !================================================================
  ! This routine computes the acceleration using analytical models.
  ! x(i,1:ndim) are cell center position in [0,boxlen] (user units).
  ! f(i,1:ndim) is the gravitational acceleration in user units.
  !================================================================
  integer::idim,i
  real(dp)::G,mmass,cmass,rmass,emass,xmass,ymass,zmass,rr,rx,ry,rz
  real(dp)::FofC,FofR

  ! Constant vector
  if(gravity_type==1)then
     do idim=1,ndim
        do i=1,ncell
           f(i,idim)=gravity_params(idim)
        end do
     end do
  end if

  ! Point mass
  if(gravity_type==2)then
     gmass=gravity_params(1) ! GM
     emass=dx
     emass=gravity_params(2) ! Softening length
     xmass=gravity_params(3) ! Point mass coordinates
     ymass=gravity_params(4)
     zmass=gravity_params(5)
     do i=1,ncell
        rx=0.0d0; ry=0.0d0; rz=0.0d0
        rx=x(i,1)-xmass
#if NDIM>1
        ry=x(i,2)-ymass
#endif
#if NDIM>2
        rz=x(i,3)-zmass
#endif
        rr=sqrt(rx**2+ry**2+rz**2+emass**2)
        f(i,1)=-gmass*rx/rr**3
#if NDIM>1
        f(i,2)=-gmass*ry/rr**3
#endif
#if NDIM>2
        f(i,3)=-gmass*rz/rr**3
#endif
     end do
  end if

  ! NFW Halo
  if (gravity_type==3)then
#if NDIM<3
     stop "NFW only in 3D
#endif
     mmass=gravity_params(1) ! M200
     emass=gravity_params(2) ! Softening length
     xmass=gravity_params(3) ! Center mass coordinates
     ymass=gravity_params(4)
     zmass=gravity_params(5)
     cmass=gravity_params(6) ! concentration
     rmass=gravity_params(7) ! virial radius

     G = 6.67d-8

     FofC = log(1.d0 + cmass) - cmass/(1.d0 + cmass)

     do i=1,ncell
        rx=0.0d0; ry=0.0d0; rz=0.0d0
        rx=x(i,1)-xmass
        ry=x(i,2)-ymass
        rz=x(i,3)-zmass

        rr=max(sqrt(rx**2+ry**2+rz**2),emass)
        FofR = log(1.d0 + rr/(rmass/cmass)) - rr/(rmass/cmass)/(1.d0 + rr/(rmass/cmass))
        
        f(i,1)=-G*mmass*rx/rr**3*FofR/FofC
        f(i,2)=-G*mmass*ry/rr**3*FofR/FofC
        f(i,3)=-G*mmass*rz/rr**3*FofR/FofC

     end do
  endif

end subroutine gravana
!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine phi_ana(rr,pp,ngrid)
  use amr_commons
  use poisson_commons
  implicit none
  integer::ngrid
  real(dp),dimension(1:nvector)::rr,pp
  ! -------------------------------------------------------------------
  ! This routine set up boundary conditions for fine levels.
  ! -------------------------------------------------------------------

  integer :: i
  real(dp):: fourpi

  fourpi=4.D0*ACOS(-1.0D0)

#if NDIM==1
  do i=1,ngrid
     pp(i)=multipole(1)*fourpi/2d0*rr(i)
  end do
#endif
#if NDIM==2
  do i=1,ngrid
     pp(i)=multipole(1)*2d0*log(rr(i))
  end do
#endif
#if NDIM==3
  do i=1,ngrid
     pp(i)=-multipole(1)/rr(i)
  end do
#endif
end subroutine phi_ana
